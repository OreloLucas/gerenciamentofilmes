unit uFrmDVDConsulta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UDMConn;

type
  TFrmDVDConsulta = class(TForm)
    Label1: TLabel;
    edtSinopse: TEdit;
    btnPesquisar: TButton;
    btnCancela: TButton;
    btnLimparPesquisa: TButton;
    procedure btnCancelaClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure btnLimparPesquisaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmDVDConsulta: TFrmDVDConsulta;

implementation

{$R *.dfm}

procedure TFrmDVDConsulta.btnCancelaClick(Sender: TObject);
begin
Close;
end;

procedure TFrmDVDConsulta.btnLimparPesquisaClick(Sender: TObject);
begin

DMConn.QrySelectGrid.Close;
with DMConn.QrySelectGrid.SQL do
begin
  clear;
  add('SELECT * FROM DVD');
end;
DMConn.QrySelectGrid.Open;

end;

procedure TFrmDVDConsulta.btnPesquisarClick(Sender: TObject);
begin
DMConn.QrySelectGrid.Close;
with DMConn.QrySelectGrid.SQL do
begin
  clear;
  add('SELECT * FROM DVD');
  add(' where UCASE(NOME) like UCASE(''%' + edtSinopse.Text +  '%'') ');
  add(' or UCASE(GENERO) like  UCASE(''%' + edtSinopse.Text +  '%'') ');
  add(' or UCASE(SINOPSE) like UCASE(''%' + edtSinopse.Text +  '%'') ');
end;
DMConn.QrySelectGrid.Open;
end;

end.
