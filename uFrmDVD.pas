unit uFrmDVD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, jpeg, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, UDMConn, AxCtrls, DB;

type
  TFrmDVD = class(TForm)
    edtNomeFilme: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Image1: TImage;
    FileOpenDialog1: TFileOpenDialog;
    Salvar: TButton;
    edtGenero: TEdit;
    Button1: TButton;
    Button2: TButton;
    DataSource1: TDataSource;
    mmSinopse: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure SalvarClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    mmStream: TMemoryStream;
    function JpgToBmp(Jpeg: TJPEGImage): TBitmap;
  public
    { Public declarations }
  end;

var
  FrmDVD: TFrmDVD;

implementation

{$R *.dfm}

procedure TFrmDVD.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TFrmDVD.Button2Click(Sender: TObject);
var
  jpeg: TJPEGImage;
  Stream: TStream;
begin

  FileOpenDialog1.Execute;
  Image1.Picture.LoadFromFile(FileOpenDialog1.FileName);

  if ExtractFileExt(FileOpenDialog1.FileName) = '.bmp' then
    mmStream.LoadFromFile(FileOpenDialog1.FileName)
  else if ExtractFileExt(FileOpenDialog1.FileName) = '.jpg' then
  begin
    jpeg := TJPEGImage.Create();
    jpeg.LoadFromFile(FileOpenDialog1.FileName);
    JpgToBmp(jpeg).SaveToStream(mmStream);
  end;

end;

procedure TFrmDVD.FormCreate(Sender: TObject);
begin
  mmStream := TMemoryStream.Create;
end;

function TFrmDVD.JpgToBmp(Jpeg: TJPEGImage): TBitmap;
var
  vBmp: TBitmap;

begin
  vBmp := TBitmap.Create;
  vBmp.Assign(Jpeg);

  result := vBmp;
end;

procedure TFrmDVD.SalvarClick(Sender: TObject);
begin
  try
    with DMConn.QryInsertDVD.SQL do
    begin
      Clear;
      add(' INSERT INTO DVD(NOME, GENERO, SINOPSE, CARTAZ )');
      add(' VALUES (:V_NOME, :V_GENERO, :V_SINOPSE, :V_CARTAZ );');
    end;

    DMConn.QryInsertDVD.Parameters.ParamByName('V_NOME').Value := edtNomeFilme.Text;
    DMConn.QryInsertDVD.Parameters.ParamByName('V_GENERO').Value := edtGenero.Text;
    DMConn.QryInsertDVD.Parameters.ParamByName('V_SINOPSE').Value := mmSinopse.Text;
    DMConn.QryInsertDVD.Parameters.ParamByName('V_CARTAZ').LoadFromStream(mmStream, ftBlob);
    DMConn.QryInsertDVD.ExecSQL;
    Application.MessageBox(PAnsiChar('DVD salvo com sucesso'), 'Sucesso', MB_OK);
    Close;
  except
    on E: Exception do
      Application.MessageBox(PAnsiChar('Erro ao salvar o DVD' + e.Message), 'ERRO', MB_ICONERROR + MB_OK);
  end;
end;

end.
