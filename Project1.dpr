program Project1;

uses
  Forms,
  uFrmDVD in 'uFrmDVD.pas' {FrmDVD},
  UDMConn in 'UDMConn.pas' {DMConn: TDataModule},
  Main in 'Main.pas' {TMainMenu},
  uFrmDVDConfirmaExclusao in 'uFrmDVDConfirmaExclusao.pas' {FrmDVDConfirmaExclusao},
  uThread in 'uThread.pas',
  uFrmDVDConsulta in 'uFrmDVDConsulta.pas' {FrmDVDConsulta};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TTMainMenu, TMainMenu);
  Application.CreateForm(TDMConn, DMConn);
  Application.CreateForm(TFrmDVDConsulta, FrmDVDConsulta);
  Application.Run;
end.
