object TMainMenu: TTMainMenu
  Left = 0
  Top = 0
  Caption = 'Menu'
  ClientHeight = 774
  ClientWidth = 1708
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  PrintScale = poPrintToFit
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 601
    Width = 1708
    Height = 173
    Align = alClient
    DataSource = DSGrid
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 25
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOME'
        Title.Caption = 'Nome DVD'
        Width = 500
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GENERO'
        Title.Caption = 'Genero'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SINOPSE'
        Title.Caption = 'Sinopse'
        Width = 1000
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1708
    Height = 601
    Align = alTop
    TabOrder = 1
    object DBImage1: TDBImage
      Left = 8
      Top = 55
      Width = 569
      Height = 540
      Align = alCustom
      DataField = 'cartaz'
      DataSource = DataSource2
      Stretch = True
      TabOrder = 0
      Visible = False
    end
    object btnStart: TButton
      Left = 16
      Top = 8
      Width = 129
      Height = 41
      Caption = 'Inicia Propaganda'
      TabOrder = 1
      OnClick = btnStartClick
    end
    object btnStop: TButton
      Left = 151
      Top = 8
      Width = 129
      Height = 41
      Caption = 'Para Propaganda'
      Enabled = False
      TabOrder = 2
      OnClick = btnStopClick
    end
  end
  object MainMenu1: TMainMenu
    Left = 16
    object Filmes1: TMenuItem
      Caption = 'DVD'
      object Novofilme1: TMenuItem
        Caption = 'Novo'
        OnClick = Novofilme1Click
      end
      object Editar1: TMenuItem
        Caption = 'Editar'
        Visible = False
      end
      object Excluir1: TMenuItem
        Caption = 'Excluir'
        OnClick = Excluir1Click
      end
      object Buscar1: TMenuItem
        Caption = 'Buscar'
        OnClick = Buscar1Click
      end
    end
  end
  object DSGrid: TDataSource
    DataSet = DMConn.QrySelectGrid
    Left = 64
    Top = 48
  end
  object DataSource2: TDataSource
    DataSet = DMConn.QryImgShow
    Left = 144
    Top = 16
  end
end
