object FrmDVDConfirmaExclusao: TFrmDVDConfirmaExclusao
  Left = 0
  Top = 0
  Caption = 'Deseja realmente excluir o DVD ?'
  ClientHeight = 404
  ClientWidth = 435
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 35
    Width = 69
    Height = 13
    Caption = 'Nome do Filme'
  end
  object Label3: TLabel
    Left = 8
    Top = 81
    Width = 35
    Height = 13
    Caption = 'Genero'
  end
  object Label2: TLabel
    Left = 216
    Top = 35
    Width = 37
    Height = 13
    Caption = 'Sinopse'
  end
  object Label5: TLabel
    Left = 8
    Top = 12
    Width = 18
    Height = 13
    Caption = 'ID :'
  end
  object Button1: TButton
    Left = 209
    Top = 371
    Width = 75
    Height = 25
    Caption = 'Sim'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 128
    Top = 371
    Width = 75
    Height = 25
    Caption = 'N'#227'o'
    TabOrder = 1
    OnClick = Button2Click
  end
  object imgCartaz: TDBImage
    Left = 8
    Top = 127
    Width = 417
    Height = 228
    DataField = 'cartaz'
    DataSource = DataSource1
    ReadOnly = True
    Stretch = True
    TabOrder = 2
  end
  object edtNome: TDBEdit
    Left = 8
    Top = 54
    Width = 202
    Height = 21
    DataField = 'NOME'
    DataSource = DataSource1
    ReadOnly = True
    TabOrder = 3
  end
  object edtID: TDBEdit
    Left = 32
    Top = 8
    Width = 105
    Height = 21
    DataField = 'ID'
    DataSource = DataSource1
    TabOrder = 4
  end
  object edtGenero: TDBEdit
    Left = 8
    Top = 100
    Width = 202
    Height = 21
    DataField = 'GENERO'
    DataSource = DataSource1
    ReadOnly = True
    TabOrder = 5
  end
  object mmSinopse: TDBMemo
    Left = 216
    Top = 54
    Width = 211
    Height = 67
    DataField = 'SINOPSE'
    DataSource = DataSource1
    ReadOnly = True
    TabOrder = 6
  end
  object DataSource1: TDataSource
    DataSet = DMConn.QrySelectGrid
    Left = 304
    Top = 8
  end
end
