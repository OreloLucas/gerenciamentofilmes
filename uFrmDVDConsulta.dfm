object FrmDVDConsulta: TFrmDVDConsulta
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Consulta'
  ClientHeight = 115
  ClientWidth = 269
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 32
    Height = 13
    Caption = 'Buscar'
  end
  object edtSinopse: TEdit
    Left = 8
    Top = 27
    Width = 250
    Height = 21
    TabOrder = 0
  end
  object btnPesquisar: TButton
    Left = 183
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Pesquisar'
    TabOrder = 1
    OnClick = btnPesquisarClick
  end
  object btnCancela: TButton
    Left = 8
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 2
    OnClick = btnCancelaClick
  end
  object btnLimparPesquisa: TButton
    Left = 89
    Top = 70
    Width = 88
    Height = 25
    Caption = 'Limpar Pesquisa'
    TabOrder = 3
    OnClick = btnLimparPesquisaClick
  end
end
