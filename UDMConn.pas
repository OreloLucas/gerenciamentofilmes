unit UDMConn;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TDMConn = class(TDataModule)
    ADOConnection1: TADOConnection;
    QrySelectGrid: TADOQuery;
    QryInsertDVD: TADOQuery;
    QryDeleteDVD: TADOQuery;
    QrySelectGridSINOPSE: TWideStringField;
    QrySelectGridGENERO: TWideStringField;
    QrySelectGridID: TAutoIncField;
    QrySelectGridNOME: TWideStringField;
    QrySelectGridcartaz: TBlobField;
    QryImgShow: TADOQuery;
    AutoIncField1: TAutoIncField;
    WideStringField1: TWideStringField;
    WideStringField2: TWideStringField;
    WideStringField3: TWideStringField;
    BlobField1: TBlobField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DMConn: TDMConn;

implementation

{$R *.dfm}

procedure TDMConn.DataModuleCreate(Sender: TObject);
var
  localDB: string;
begin
  QrySelectGrid.Connection := ADOConnection1;
  QryInsertDVD.Connection := ADOConnection1;
  QryImgShow.Connection := ADOConnection1;
  QryDeleteDVD.Connection := ADOConnection1;
  localDB := ExtractFilePath(ParamStr(0)) + 'projeto1.mdb';
  ADOConnection1.ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Password="";Data Source=' +
    localDB + ';Persist Security Info=True';
  ADOConnection1.Connected := true;
  ADOConnection1.LoginPrompt := false;
end;

end.
