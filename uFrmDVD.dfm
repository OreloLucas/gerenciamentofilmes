object FrmDVD: TFrmDVD
  Left = 0
  Top = 0
  Caption = 'Novo DVD'
  ClientHeight = 387
  ClientWidth = 487
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 5
    Width = 69
    Height = 13
    Caption = 'Nome do Filme'
  end
  object Label2: TLabel
    Left = 199
    Top = 5
    Width = 37
    Height = 13
    Caption = 'Sinopse'
  end
  object Label3: TLabel
    Left = 8
    Top = 53
    Width = 35
    Height = 13
    Caption = 'Genero'
  end
  object Image1: TImage
    Left = 8
    Top = 99
    Width = 376
    Height = 278
    Stretch = True
  end
  object edtNomeFilme: TEdit
    Left = 8
    Top = 24
    Width = 185
    Height = 21
    TabOrder = 0
  end
  object Salvar: TButton
    Left = 404
    Top = 321
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 1
    OnClick = SalvarClick
  end
  object edtGenero: TEdit
    Left = 8
    Top = 72
    Width = 185
    Height = 21
    TabOrder = 2
  end
  object Button1: TButton
    Left = 404
    Top = 352
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 404
    Top = 99
    Width = 75
    Height = 25
    Caption = 'Abrir imagem'
    TabOrder = 4
    OnClick = Button2Click
  end
  object mmSinopse: TMemo
    Left = 199
    Top = 24
    Width = 185
    Height = 69
    TabOrder = 5
  end
  object FileOpenDialog1: TFileOpenDialog
    DefaultExtension = '*.jpeg, *.bmp'
    FavoriteLinks = <>
    FileTypes = <
      item
        DisplayName = '*.bmp;*.jpg'
        FileMask = '*.bmp;*.jpg'
      end>
    Options = [fdoStrictFileTypes]
    Left = 399
    Top = 91
  end
  object DataSource1: TDataSource
    DataSet = DMConn.QrySelectGrid
    Left = 424
    Top = 168
  end
end
