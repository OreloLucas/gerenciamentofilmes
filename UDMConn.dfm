object DMConn: TDMConn
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 296
  Width = 607
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Password="";Data Source=C:\User' +
      's\Lucas Orelo\Documents\projeto1\projeto1.mdb;Persist Security I' +
      'nfo=True'
    KeepConnection = False
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 56
    Top = 16
  end
  object QrySelectGrid: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM DVD')
    Left = 56
    Top = 88
    object QrySelectGridID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QrySelectGridNOME: TWideStringField
      FieldName = 'NOME'
      Size = 255
    end
    object QrySelectGridSINOPSE: TWideStringField
      FieldName = 'SINOPSE'
      Size = 255
    end
    object QrySelectGridGENERO: TWideStringField
      FieldName = 'GENERO'
      Size = 255
    end
    object QrySelectGridcartaz: TBlobField
      FieldName = 'cartaz'
    end
  end
  object QryInsertDVD: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    Left = 56
    Top = 152
  end
  object QryDeleteDVD: TADOQuery
    Parameters = <>
    Left = 48
    Top = 224
  end
  object QryImgShow: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM DVD')
    Left = 168
    Top = 88
    object AutoIncField1: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object WideStringField1: TWideStringField
      FieldName = 'NOME'
      Size = 255
    end
    object WideStringField2: TWideStringField
      FieldName = 'SINOPSE'
      Size = 255
    end
    object WideStringField3: TWideStringField
      FieldName = 'GENERO'
      Size = 255
    end
    object BlobField1: TBlobField
      FieldName = 'cartaz'
    end
  end
end
