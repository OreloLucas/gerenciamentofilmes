unit uFrmDVDConfirmaExclusao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UDMConn, DBCtrls, Mask, DB;

type
  TFrmDVDConfirmaExclusao = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    imgCartaz: TDBImage;
    edtNome: TDBEdit;
    DataSource1: TDataSource;
    edtID: TDBEdit;
    edtGenero: TDBEdit;
    mmSinopse: TDBMemo;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmDVDConfirmaExclusao: TFrmDVDConfirmaExclusao;

implementation

{$R *.dfm}

procedure TFrmDVDConfirmaExclusao.Button1Click(Sender: TObject);
begin
  try
    if MessageDlg('Deseja realmente deletar o DVD ?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      exit;

    with DMConn.QryDeleteDVD.SQL do
    begin
      Clear;
      add('delete from DVD where id = ' + edtID.Text);
    end;
    DMConn.QryDeleteDVD.ExecSQL;
    Close;

  except
    on E: Exception do
      Application.MessageBox(PAnsiChar('Erro ao salvar o DVD' + e.Message), 'ERRO', MB_ICONERROR + MB_OK);

  end;
end;

procedure TFrmDVDConfirmaExclusao.Button2Click(Sender: TObject);
begin
  Close;
end;

end.
