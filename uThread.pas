unit uThread;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, Grids, DBGrids, DB, ADODB, UDMConn, uFrmDVD, uFrmDVDConfirmaExclusao,
  jpeg, DBCtrls, StdCtrls;

type
  TPropagandaThread = class(TThread)
  protected

  public
    procedure Execute; override;
    constructor Create;
  end;

implementation

constructor TPropagandaThread.Create;
begin
  inherited Create(true);
end;

procedure TPropagandaThread.Execute;
begin
  inherited;
  while not Self.Terminated do
  begin
    sleep(10000);
    Application.ProcessMessages;
    
     if DMConn.QryImgShow.EOF then
     begin
       DMConn.QryImgShow.Close;
       DMConn.QryImgShow.Open;
       DMConn.QryImgShow.First
     end
    else
      DMConn.QryImgShow.Next;
  end;

end;

end.
