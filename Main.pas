unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Menus, Grids, DBGrids, DB, ADODB, UDMConn, uFrmDVD, uFrmDVDConfirmaExclusao,
  jpeg, DBCtrls, StdCtrls, uThread, uFrmDVDConsulta;

type
  TTMainMenu = class(TForm)
    DBGrid1: TDBGrid;
    MainMenu1: TMainMenu;
    Filmes1: TMenuItem;
    Novofilme1: TMenuItem;
    Editar1: TMenuItem;
    Excluir1: TMenuItem;
    DSGrid: TDataSource;
    Panel1: TPanel;
    DBImage1: TDBImage;
    DataSource2: TDataSource;
    btnStart: TButton;
    btnStop: TButton;
    Buscar1: TMenuItem;
    procedure Novofilme1Click(Sender: TObject);
    procedure Excluir1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure Buscar1Click(Sender: TObject);
  private
    { Private declarations }
    tt: TPropagandaThread;
  public
    { Public declarations }
  end;

var
  TMainMenu: TTMainMenu;

implementation

{$R *.dfm}

procedure TTMainMenu.btnStartClick(Sender: TObject);
begin
  DMConn.QryImgShow.Open;
  DMConn.QryImgShow.First;
   DBImage1.Visible := true;
  tt := TPropagandaThread.Create;
  tt.FreeOnTerminate := true;
  tt.Resume;
  btnStop.Enabled := true;
end;

procedure TTMainMenu.btnStopClick(Sender: TObject);
begin
  tt.Suspend;
  DBImage1.Visible := false;
  DMConn.QryImgShow.Close;
  btnStop.Enabled := false;
end;

procedure TTMainMenu.Buscar1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmDVDConsulta, FrmDVDConsulta);
  FrmDVDConsulta.ShowModal;
  FreeAndNil(FrmDVDConsulta);
end;

procedure TTMainMenu.Excluir1Click(Sender: TObject);
begin
  if DMConn.QrySelectGrid.IsEmpty then
  begin
    Application.MessageBox(PAnsiChar('N�o existe DVD para ser excluido'), 'Info', MB_ICONEXCLAMATION + MB_OK);
    exit;
  end;
  Application.CreateForm(TFrmDVDConfirmaExclusao, FrmDVDConfirmaExclusao);
  FrmDVDConfirmaExclusao.ShowModal;
  FreeAndNil(FrmDVDConfirmaExclusao);
  DMConn.QrySelectGrid.Close;
  DMConn.QrySelectGrid.Open;
end;

procedure TTMainMenu.FormShow(Sender: TObject);
begin
  DMConn.QrySelectGrid.Open;

end;

procedure TTMainMenu.Novofilme1Click(Sender: TObject);
begin
  Application.CreateForm(TFrmDVD, FrmDVD);
  FrmDVD.ShowModal;
  FreeAndNil(FrmDVD);
  DMConn.QrySelectGrid.Close;
  DMConn.QrySelectGrid.Open;
end;

end.
